using System;

public class EstructurasDeControl 
{
	public static void Main() 
	{
		/*EjemploIfSimple();
		EjemploIfComplicado();
		EjercicioIfSumas();
		
		EjemploIfConsecutivos();
		*/
		EjemploBucleWhile();
	}
	static void EjemploIfSimple()
	{
		// Condicional simple:	if (booleano) instruccion;
		if (true) Console.WriteLine("Pues sí");
		if (false) Console.WriteLine("Pues va a ser que no");
		
		// Recibe variable
		bool oSioNo = true;
		if (oSioNo) Console.WriteLine("Pues también sí");
		// O recibimos condicionales
		if (5 == 5) Console.WriteLine("Pues 5 == 5");
		if (4 > 7) Console.WriteLine("Pues esto tampoco se muestra");
		
		
	}
	
	
	static void EjemploIfComplicado()
	{
		// IF complicado: 
		// if (bool) instruccionVerdad; else instruccionFalso;
		if (4 >= 7) Console.WriteLine("4 >= 7"); 
		else Console.WriteLine("4 < 7");
		// Podemos separar en varias lineas
		if ("Hola" != "hola") Console.WriteLine("Son dist"); 
		else Console.WriteLine("Son =");
		
	}
	static void EjercicioIfSumas()
	{
		// Ejercicio:
		string numA = "20", numB = "30", numC = "40";
		int resultado = 50;
		// Suma las 3 combinaciones (A + B,  B+C y A+C) y que el programa diga cual
		// es igual a resultado.
		// Consola debe mostrar:
		// 1ro mostrar los valores: A = 20, B = 30, C = 40, resultado = 50
		// A + B es igual a resultado
		// A + C es distinto de resultado
		// B + C es distinto de resultado
		
		// Solucion:
		int intAA, intBB, intCC;
		intAA = Int32.Parse(numA);
		intBB = Int32.Parse(numB);
		intCC = Int32.Parse(numC);
		
		if (intA + intB == resultado)
			Console.WriteLine(" A + B es igual al resultado");
		else
			Console.WriteLine(" A + B es distinto de resultado");
		
		if (intA + intC == resultado)
			Console.WriteLine(" A + C es igual al resultado");
		else
			Console.WriteLine(" A + C es distinto de resultado");
		
		if (intB + intC == resultado)
			Console.WriteLine(" B + C es igual al resultado");
		else
			Console.WriteLine(" B + C es distinto de resultado");
		
	}
	
	static void EjemploIfConsecutivos()
	{
		Console.WriteLine("Introduzca dos opciones: ");
		Console.WriteLine("1 - Opcion primera");
		Console.WriteLine("2 - Opcion segunda");
		Console.WriteLine("3 - Opcion tercera");
		Console.WriteLine("(*) - Cualquier otra opcion");
		
		ConsloeKeyInfo opcion= Console.ReadKey;
		ConsoleKey charOpcion = opcion.Key;
		if (charOpcion == '1')
			Console.WriteLine("Has elegido la opcion primera");
		else if (opcion == '2')
			Console.WriteLine("Has elegido la opcion segunda");
	}
	// Con este bucle se puede crear cualquier tipo de bucle.
	// Puede que nunca se ejecute si el boleano es false de entrada,
	// o puede que sea un bucle infinito si la condicion siempre es true,
	// o un bucle normal con una duración determinada.
	static void EjemploBucleWhile()
	{
		Consloe.WriteLine("Antes de bucle")
			
			while (false)
			{
				Console.WriteLine("Instruccion que se repite");
			}
	
			int contador = 0;
			while (contador < 10)
			{	
				Console.WriteLine("Contador = " + contador);
				contador = contador +1;
			}
			bool sicontinuar = true;
			while (siContinuar)
			{
				Console.WriteLine("Estamos en el bucle. ¿Desea salir?");
				string tecla = Console.ReadKey().Key.ToString();
				if (tecla == "S" || tecla == "s");
				{
					siContinuar = false;
				}
			}
		
			Console.WriteLine("Despues del bucle");
	}
	static void EjemploBucleFor()
	{
		//Un bucle for es un bucle while que se suele usar como un contador
		// for ( <inicializacion> ; <condicion> ; <incremento> )
		// insctruccion; o bien un (bloque que se repite)
		for (int contador = 0 ; contador < 10 ; contador = contador + 1)
		{
			Console.WriteLine("Contador = " + contador);
		}
	}
	
	static void EjemploBucleDoWhile();
	{
		//Es como bucle while, pero se ejecuta si o si al menos una vez.
		do
		{
			Console.WriteLine("Al menos una vez");
		} while (false);
			
		bool siSalir = true	
		do 
		{
			Con

		siSalir = ! siSalir;
			
		} while ( ! siSalir);
	}
	
	
	
}










