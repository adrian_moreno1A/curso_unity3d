//Al principio se ponen las importaciones
using System;

public class HolaMundo
{
	static void Main()	{
		Console.Beep();
		{
			Console.WriteLine("HolaMundo");
			Console.WriteLine("¿Qué tal?");
			Console.ReadKey();
		}
		Console.WriteLine("Bien");
		Console.Beep();
		// tipo nombre;
		byte unNumero; // byte de 0 a 255.
		
		unNumero = 10;
		Console.WriteLine("El byte es " + unNumero);
		
		char unCaracter; // O bien 1 ò 2 bytes.
		unCaracter = 'A'; // comillas simples para caracteres.
		Console.WriteLine("El caracter es " + unCaracter);
		// Un caracter tambien es un número.
		unCaracter = (char) (65 + 4); // un char es un numero, pero
								// para hacer la conversión hay
								// que hacer "casting" que significa
								//convertir a otra cosa.
		Console.WriteLine("El caracter es " + unCaracter);
	}
}