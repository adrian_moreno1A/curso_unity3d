using System;

public class Funciones 
{
	public static void Main()
	{
		// Para usar una funcion ponemos el nombre y entre parentesis los argumentos
		//Ponemos las variables para mostrar.
		
		double y;
		string valorUsuario;
		double valUsu;
		
		
		Console.WriteLine("Introduzca valor: ");
		valorUsuario = Console.ReadLine();
		valUsu = Double.Parse(valorUsuario);
		y = FuncionLinea12x3(valUsu);
		Console.WriteLine("Resultado 2*x + 3: " + y);
		
		
		double z;
		
		Console.WriteLine("Introduzca valor: ");
		valorUsuario = Console.ReadLine();
		valUsu = Double.Parse(valorUsuario);
		z = FuncionLinea33x3(valUsu);
		Console.WriteLine("Resultado x*x + 1: " + z);
		
	
		
		
	}
	// Funciones estaticas
	//	<Modificador acceso> static <tipo dato resultado> <nombre de la funcion> (<tipo>, parametros, <tipos>, parametros2...)

	//	y luego entre llaves el cuerpo de la funcion
	// con return podemos devolver un valor
	private static double FuncionLinea12x3 (double x)
	{
			return x * 2 + 3;
			
			
	}
	private static double FuncionLinea33x3 (double x)
	{
			return x * x + 1;
			
			
	}
	


}