using System;

public class Ejercicio4
{
//Ejercicio 4: Otra que devuelva el ataque maximo del array.
	public static void Main()
	{
		// Declaro la array y la cargo
		float[] ataques = {3.2f, 1.7f, 2.4f, 5.0f, 7.1f, 4.8f};
		//Llamo la función
		CalcAtaqueMaximo();
		
	}
	
	public static void CalcAtaqueMaximo(float[] ataques) 
	{
		// Declaro que float es 0 para compararlo con el maximo
		float numMax = ataques[0];
		
		// Recorro el array para buscar el maximo
		for (int i = 0; i < ataques.Length; i ++)
		{
			
			//Comparo cada elemento con el numero maximo a mostrar
			if (ataques[i]> numMax)
			{
				// Si es mayor lo introduzco en la variable para poder compararlo
				numMax = ataques[i];
			}				
		}
		// Muestro el numero maximo
		Console.WriteLine("Valor maximo es: " + numMax);
	}
}