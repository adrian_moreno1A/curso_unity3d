﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour
{
    public GameObject[] enemigos;
    public AparicionEnemigo[] aparicionesEnem;

   /* public GameObject prefabSprite_lata;
    public GameObject prefabSprite_Botella;
    public GameObject prefabSprite_Plastico;
    */

    public int vidas = 7;
    public int puntos = 0;
    public GameObject textoVidas;
    public GameObject textoPuntos;

    private float timeIni;
    private int enemActual;

    //private int puntosAnt; //Puntos del frame anterior

    // Start is called before the first frame update
    void Start()
    {
        // Aproximarse a mitad de probabilidades
        /*  if (Random.Range(0, 2)==0)
          {
              GameObject.Instantiate(prefabSprite_lata);
          }

         else
         {
                  GameObject.Instantiate(prefabSprite_Botella);
         }
         */
        // this.InstanciarEnemigo();

        timeIni = Time.time;
        enemActual = 0;

    }

    // Update is called once per frame
    void Update()
    {
        this.textoVidas.GetComponent<Text>().text
            = "Vidas: " + this.vidas;

        this.textoPuntos.GetComponent<Text>().text
            = "Puntos: " + this.puntos;
        // Para saber si un enemigo tiene que aparecer,
        //tenemos que saber cuano tiempo ha pasado desde
        // inicio de nivel hasta el momento actual (frame actual),
        // y si es superior al tiempo configurado en la aparicion de enemigo
        float tiempoActual = Time.time - timeIni;
        if (tiempoActual > aparicionesEnem[enemActual].tiempoInicio)
        {
            // Si no ha aparecido...
            if ( ! aparicionesEnem[enemActual].yaHaAparecido)
            {
                this.InstanciarEnemigo();
                aparicionesEnem[enemActual].yaHaAparecido = true;
                enemActual++;
            }
            
        }

        /* if (this.puntos != this.puntosAnt)
        {
            this.InstanciarEnemigo();
        }

        this.puntosAnt = this.puntos; */
    }


    // Esto es un nuevo metodo (accion,conjunto de instrucciones, función, procedimiento, mensaje...)
    public void CuandoCapturamosEnemigo()
    {
        // Estamos dando un nuevo valor a la puntuación
        this.puntos = this.puntos + 10;     // this.puntos += 10;

        //this.InstanciarEnemigo();


    }
    public void CuandoPerdemosUnEnemigo()
    {
        this.vidas = this.vidas - 1;    // this.vidas -= 1; this.vidas--;

        //this.InstanciarEnemigo();
        

     }
    public void InstanciarEnemigo()
    {
       /*   int numEnemigo = Random.Range(0, 3);
         if (numEnemigo == 0)
         {
             GameObject.Instantiate(prefabSprite_lata);
         }

         else if (numEnemigo ==1)
         {
             GameObject.Instantiate(prefabSprite_Botella);
         }

         else if (numEnemigo == 2)
         {
             GameObject.Instantiate(prefabSprite_Plastico);
         }
         */

         int numEnemigo = Random.Range(0, enemigos.Length);

             GameObject.Instantiate (enemigos[numEnemigo]);
                
          

    }

}
