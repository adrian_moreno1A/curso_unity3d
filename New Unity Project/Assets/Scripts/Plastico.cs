﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plastico : MonoBehaviour
{
    // Start is called before the first frame update

    public float velocidad = 10;

    GameObject jugador;

    //Start se llama la primer avez
    private void Start()
    {
        float posInicioX = Random.Range(-10, 10);

        this.transform.position = new Vector3(posInicioX, 10, 1);

        // Referencia al objeto

        jugador = GameObject.Find("Jugador_Caballito");

    }
    // Update is called once per frame
    void Update()
    {
        // Damos al vector el movimiento hacia abajo
        // una vez dado, se transforma la posicion.

        Vector3 movAbajo = velocidad
            * new Vector3(0, -1, 0)
            * Time.deltaTime;

        // Transformamos la posicion con un position + movAbajo

        this.GetComponent<Transform>().position =
            this.GetComponent<Transform>().position
            + movAbajo;

        // Para saber si ha llegado abajo, preguntamos
        // si al obtener la posicion es mayor que el margen de abajo

        if (this.GetComponent<Transform>().position.y <= 0)
        {



            this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, -6, 1);
            // Colisiones de objetos (caballito y lata)
            if (this.transform.position.x >= jugador.transform.position.x - 3.2f / 2
                && this.transform.position.x <= jugador.transform.position.x + 3.2f / 2)
            {
                GameObject.Find("ControladorJuego")
                    .GetComponent<ControladorJuego>()
                    .CuandoCapturamosEnemigo();

                Destroy(this.gameObject);

            }
            else
            {
                GameObject.Find("ControladorJuego")
                    .GetComponent<ControladorJuego>()
                    .vidas -= 1;

                GameObject.Find("ControladorJuego")
                    .GetComponent<ControladorJuego>()
                    .CuandoPerdemosUnEnemigo();


                Destroy(this.gameObject);
            }
        }



    }
}
