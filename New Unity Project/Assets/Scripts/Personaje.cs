﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public float velocidad = 40;
    // Update is called once per frame
   // float velocidad;    // float es el numero decimal
    //velocidad = 50; // 40 es el valor por defecto

    //Update is called once per frame
    void Update()
        
    {
       
        //Cuando se pulsa izqierda

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            // Creamos el vector de movimiento a partir del calculoqu
            // influye la velocidad (40), el vector hacia la izquierda (-1, 0, 0): (-40, 0, 0)
            // Para que se mueva -40 unid. por segundo en vez de por cada frame.
            // multiplicamos por el incremento del tiempo (aprox. 0.02 seg, 50FPS) (0.8, 0, 0)
            //multiplicamos por el incremento del tiempo (aprox. 0.02 seg. 50 FPS
                    //      unid / seg  x   1   x seg/frame =   unidades/frame

            Vector3 vectorMov = velocidad * Vector3.left * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMov);

            //Si la posición en el eje X es menor que  -10 en el margen izquierdo  

            if(this.GetComponent<Transform>().position.x < -10 )
            {
                //entonces recolocamos en el margen izquierdo

                this.GetComponent<Transform>().position = new Vector3(-10, 0, 0);
                Debug.Log("Hemos chocado a la izquierda: ");
            }
        }

        //Cuando se pulsa a la derecha

       if (Input.GetKey(KeyCode.RightArrow))
        {
            Vector3 vectorMov = velocidad * Vector3.right * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMov);

            if (this.GetComponent<Transform> ().position.x > +10)
            {
                //entonces recolocamos en el margen derecho

                this.GetComponent<Transform>().position = new Vector3(+10, 0, 0);
                Debug.Log("Hemos chocado a la izquierda: ");
            }
        }
       /*
        if (Input.GetKey(KeyCode.UpArrow))
        {
            this.GetComponent<Transform>().Translate(Vector3.up);

        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            this.GetComponent<Transform>().Translate(Vector3.down);

        }
        */
    }
}